import React, { Component } from 'react';
import {FeatureViewer} from "feature-viewer-typescript/lib";
import 'feature-viewer-typescript/assets/fv.scss'

export default class FeatureViewerComponent extends Component {
    initViewer = (divId) => {
        const P04050 = 'MTKFTILLISLLFCIAHTCSASKWQHQQDSCRKQLQGVNLTPCEKHIMEKIQGRGDDDDDDDDDNHILRTMRGRINYIRRNEGKDEDEEEEGHMQKCCTEMSELRSPLMTKFTILLISLLFCIAHTCSASKWQHQQDSCRKQLQGVNLTPCEKHIMEKIQGRGDDDDDDDDDNHILRTMRGRINYIRRNEGKDEDEEEEGHMQKCCTEMSELRSPLMTKFTILLISLLFCIAHTCSASKWQHQQDSCRKQLQGVNLTPCEKHIMEKIQGRGDDDDDDDDDNHILRTMRGRINYIRRNEGKDEDEEEEGHMQKCCTEMSELRSPL';
        const fv = new FeatureViewer(P04050, '#' + divId, {
            toolbar: true,
            toolbarPosition: 'left',
            brushActive: true,
            zoomMax: 10,
            flagColor: '#DFD5F5'
        });
        return fv;
    }
    viewViewer = (divId) => {
        const fv = this.initViewer("fvDivInit");
        const featurelist = [
            {
                type: 'rect',
                id: 'mysimplefeature',
                data: [{x: 50, y: 100}],
                sidebar: [
                    {
                        id: 'MyPercentage',
                        type: 'percentage',
                        label: 50
                    },
                    {
                        id: 'MyHtml',
                        content: '<button class="btn btn-primary">Button</button>',
                        tooltip: 'Something in tooltip'
                    },
                    {
                        id: 'UniProt',
                        type: 'button',
                        label: 'UniProt',
                        tooltip: '<b>Very</b> <span style="color: #C21F39">Dynamic</span> <span style="color: #00b3ee">Reusable</span>\n' +
                            '<b><i><span style="color: #ffc520">Tooltip With</span></i></b> <small>Html support</small>'
                    },
                    {
                        id: 'Link',
                        type: 'icon',
                        label: 'M8 1.88v-1.88h2v16h10l-4 4h-14l-2-4h8v-2h-8v-0.26c3.736-3.009 6.529-7.050 ' +
                            '7.955-11.69l0.045-0.17zM19.97 14h-9.97v-0.36c1.263-1.865 2.017-4.164 2.017-6.64s-' +
                            '0.753-4.775-2.043-6.682l0.027 0.042v-0.2c5.609 2.315 9.566 7.57 9.968 13.793l0.002 0.047z'
                    }
                ],
                isOpen: true,
                subfeatures: [
                    {
                        type: 'rect',
                        data: [
                            {x: 20, y: 30},
                            {x: 15, y: 45},
                            {x: 70, y: 100, label: 'myRect', tooltip: 'myTooltip'}
                        ],
                        id: 'aDifferentId',
                        label: 'subfeature',
                        subfeatures: [
                            {
                                type: 'lollipop',
                                data: [
                                    {x: 20, y: 1},
                                    {x: 70, y: 0.15, label: 'myLollipop', tooltip: 'myTooltip'}
                                ],
                                id: 'subfeatureOfsubfeature',
                                label: '<div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap">My very long label should have ellipses</div>>'
                            }
                        ]
                    }
                ]
            }
       ];
        fv.addFeatures(featurelist)
    }
    componentDidMount() {
        this.viewViewer("fvDivInit")
    }
    render() {
        return(
            <div id="fvDivInit"/>
        )
    }
}