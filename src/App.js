import './App.css';
import FeatureViewerComponent from "./FeatureViewerComponent";

function App() {
  return (
    <div className="App">
        <FeatureViewerComponent />
    </div>
  );
}

export default App;
